package com.ascend.project.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

	@RequestMapping("/index")
	public String index(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
		if (null != name) {
			model.addAttribute("name", name);
		}
		return "index";
	}

}
